import com.sun.org.apache.xpath.internal.operations.Bool
import java.util.*

/*
 * inline function.
 */
inline fun doSomeWork(func : () -> String) {
    println(func())
}

/**
 * extension function for Ints for print value and do some work on Int
 */
inline fun Int.printMeAndDoSomeWork(func : () -> Unit) {
    println(this)
    func()
}

/**
 * print some int with lambda function printMeAndDoSomeWork for showing not-local return with inline function
 */
fun printSomeInts(list : MutableList<Int>) {

    list.forEach {
        it.printMeAndDoSomeWork {
            if (it == 5) {
                println("iam exit now")
                return
            }
        }
    }
}

/**
 * reified in inline function
 * if do not use reified keyword for T generic we get syntax error for --ParentType-- is --Generic--
 */

inline fun <reified T> seanceClassTypes() {

    // uses value for seance class type because values is initialize
    when {
        "" is T -> {
            println("its String")
        }
        0 is T -> {
            println("its Int")
        }
        13.4F is T -> {
            println("its Float")
        }
        true is T -> {
            println("its Boolean")
        }
        else -> {
            println("function does not known this type")
        }
    }
}

inline fun <reified T> membersOf() = T::class.members

fun main() {

    /**
     * call inline function.
     */
    doSomeWork {
        "hello world"
    }

    /**
     * example for Non-local returns
     */
    printSomeInts(mutableListOf<Int>().apply {
        add(1)
        add(2)
        add(3)
        add(4)
        add(5)
        add(6)
        add(7)
    })

    /**
     * example for reified
     */

    seanceClassTypes<String>()
    seanceClassTypes<Bool>()
    seanceClassTypes<Base64>()
    println("member of Boolean is : ${membersOf<Boolean>().joinToString("\n")}")
}